# import json
# import math
# import os
# import cv2
# from PIL import Image
# import numpy as np
from keras import layers
# from keras.callbacks import Callback, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
# from keras.preprocessing.image import ImageDataGenerator
# from keras.utils.np_utils import to_categorical
from keras.models import Sequential
from keras import optimizers
from keras.optimizers import Adam
from keras.applications.resnet import ResNet50
# from keras.applications.mobilenet import MobileNet
# from keras.applications.densenet import DenseNet201
# from keras.applications.inception_v3 import InceptionV3
# from keras.applications.nasnet import NASNetLarge, NASNetMobile
# from keras.applications.inception_resnet_v2 import InceptionResNetV2
# import matplotlib.pyplot as plt
# import pandas as pd
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import cohen_kappa_score, accuracy_score
# import scipy
# from tqdm import tqdm
import tensorflow as tf
from keras import backend as K
import gc
# from functools import partial
# from sklearn import metrics
# from collections import Counter
# import json
# import itertools
# %matplotlib inline


def build_model(backbone, lr=5e-4):
    model = Sequential()
    model.add(backbone)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dropout(0.5))
    model.add(layers.BatchNormalization())
    model.add(layers.Dense(2, activation='softmax'))

    model.compile(
        loss='binary_crossentropy',
        optimizer=Adam(learning_rate=lr),
        metrics=['accuracy']
    )

    return model


def init_model():
    K.clear_session()
    gc.collect()

    resnet = ResNet50(
        weights='imagenet',
        include_top=False,
        input_shape=(224,224,3)
    )

    model = build_model(resnet ,lr = 1e-4)
    model.summary()

    model.load_weights("weights.best.hdf5")
    return model


def predict(model,X):
    Y_pred = model.predict(X)
    return Y_pred
