# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import telebot
import config
import photo_conversion
import keras_model

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    bot = telebot.TeleBot(config.TOKEN)
    model = keras_model.init_model()


    @bot.message_handler(content_types=['text'])
    def lala(message):
        bot.send_message(message.chat.id, 'Добрый день! Это учебный бот, демонстрирующий работу нейросети, распознающей'
                                          ' рак кожи. Пришлите фотографию родинки, чтобы проверить функционирование')


    @bot.message_handler(content_types=['photo'])
    def photo_answer(message):
        src = message.from_user.username + '.png'
        file_info = bot.get_file(message.photo[0].file_id)
        downloaded_photo = bot.download_file(file_info.file_path)
        with open(src, 'wb') as new_file:
            new_file.write(downloaded_photo)
            new_file.close()
        bot.send_message(message.chat.id, 'Ваше сообщение успешно отправлено нашим индийским коллегам, иммитирующим нейросеть. '
                                          'Результат будет доступен через некоторое время.')
        img = photo_conversion.convert_photo(src, 224)

        result = keras_model.predict(model, img)
        good = '{:.3f}'.format(100 * result[0][0])
        bad = '{:.3f}'.format(100 * result[0][1])
        answer = 'Заключение нейросети: \n' + "Доброкачественная на " + good + "%\nЗлокачественная на " + bad + "%"

        bot.send_message(message.chat.id, answer)
        if bad>good:
            answer = 'Не переживайте, настоящее заключение должен вынести врач. Ответ бота не является диагнозом'
            bot.send_message(message.chat.id, answer)

bot.polling(none_stop=True)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
