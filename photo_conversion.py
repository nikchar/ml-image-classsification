# import os
import cv2
from PIL import Image
import numpy as np
# from tqdm import tqdm
import tensorflow as tf


def convert_photo(photo_name, RESIZE, sigmaX=10):
    img = np.asarray(Image.open(photo_name).convert("RGB"))
    img = cv2.resize(img, (RESIZE, RESIZE))
    img = img.reshape(1, 224, 224, 3)
    return img



